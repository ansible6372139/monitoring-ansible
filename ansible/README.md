# Monitoring Setup Ansible

This ansible contains ansible roles for server side setup of monitoring and client side setup of exporters. This setup works with VMs. For Kubernetes based setup, use helm charts 

***Server Side Setup***
    
    * Grafana
    * Promethes
    * Alertmanager
    * Blackbox

***Client ( Exporters ) Side Setup***
    
    * apache-exporter
    * elasticsearch-exporter
    * cloudwatch-exporter
    * gcp-stackdriver-exporter
    * mongodb-exporter
    * mysql-exporter
    * node-exporter
    * postgres-exporter
    * process-exporter
    * redis-exporter
    * haproxy-exporter
    * nginx-exporter


### Prerequisites
1) Instances must be running on which server / client installation needs to done
2) Instances must be tagged appropriately.
3) Host files used by ansible must be dynamically setup or create a manual one.  
4) ***Update the vars/main.yml  and vars/mo-vault.yml file as per the environment / requirement. (This is the most critical part of the setup)***
5) Host from which ansible is executed must have "Tags/Labels view permission" for dynamic inventory to work. This can be provided by IAM roles or IAM access key and secret key

## AWS AutoDiscovery (Optional)

### To enable AWS AutoDiscovery, add below line in ansible.cfg

[inventory]     
enable_plugins = aws_ec2

### Update the inventory file aws_inventory.yaml as required with filters
```
---
plugin: aws_ec2
filters:
  instance-state-name : running
  tag:Environment:
    - prod
    - preprod

groups:
  redis: "'redis' in tags.monitor"
  mysql: "'mysql' in tags.monitor"
  nodeexporter: "'yes' in tags.monitor"
  monserver: "'mon-server' in tags.monitor"
hostnames:
  - private-dns-name
  - private-ip-address
```

Above will group hosts based on values used in monitor tag for EC2, and will install the respective exporter accordingly. Here we are filtering based on running instances and Environment tag as prod and preprod only.

### Another example of the inventory file which only looks for instance with tag key as Name and tag value as gitlab and Ansible-Testing.

```
---
plugin: aws_ec2
regions:
  - ap-south-1

filters:
  instance-state-name : running
  tag:Name:
    - Ansible-Testing
    - gitlab

keyed_groups:
  - key: tags
    prefix: tag
  - prefix: instance_type
    key: instance_type
  - key: placement.region
    prefix: aws_region
```

Below is the output of the above inventory file.

###
```
command ->  ansible-inventory --graph -i /opt/ansible/inventory/aws_ec2.yaml
@all:
  |--@aws_ec2:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |  |--ec2-13-235-134-128.ap-south-1.compute.amazonaws.com
  |--@aws_region_ap_south_1:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |  |--ec2-13-235-134-128.ap-south-1.compute.amazonaws.com
  |--@instance_type_m5a_large:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@instance_type_t3_small:
  |  |--ec2-13-235-134-128.ap-south-1.compute.amazonaws.com
  |--@tag_Backup_True:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@tag_Name_Ansible_Testing:
  |  |--ec2-13-235-134-128.ap-south-1.compute.amazonaws.com
  |--@tag_Name_gitlab:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@tag_Org_techpartner:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@tag_Owner_TP:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@tag_Owner_bhavesh_patel:
  |  |--ec2-13-235-134-128.ap-south-1.compute.amazonaws.com
  |--@tag_Test_EC2_CLI_Key_test_threough_aws_cli_EC2:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@tag_Uptime_yes:
  |  |--ec2-13-232-0-150.ap-south-1.compute.amazonaws.com
  |--@ungrouped:

```
###


## GCP AutoDiscovery (Optional)
### To enable GCP AutoDiscovery, follow below steps

*  Install google-auth module using pip.
```
$ sudo pip install requests google-auth
```
* Create a service account file from GCP IAM Service accounts 

* Update the GCP inventory file gcp_inventory.yaml from repo as required with filters
```
---
plugin: gcp_compute
projects:
  - tp-gcp-project
auth_kind: serviceaccount
service_account_file: service-account.json
scopes:
 - 'https://www.googleapis.com/auth/cloud-platform'
 - 'https://www.googleapis.com/auth/compute.readonly'
groups:
  prod: "'prod' in (labels|list)"
  preprod: "'preprod' in (labels|list)"
  monserver: "'monserver' in (labels|list)"
  mysql: "'mysql' in (labels|list)"
  node-exporter: "'monitor' in (labels|list)"
  redis: "'redis' in (labels|list)"
hostnames:
  - private_ip
```
 
## Testing AWS / GCP dynamic inventory filters

`ansible-inventory -i aws_ec2.yaml --graph -vvvv`


## Playbook Execution

* Updated the vars/main.yaml file as per the environment

* Create the vault file monitoring-vault.yml file as per the environment.
```
    $ ansible-vault create mo-vault.yml  ## ask password at the time of creation which will use to edit and view the encrypted file
    $ ansible-vault edit mo-vault.yml
    $ ansible-vault view  mo-vault.yml
    $ ansible-vault decrypt   mo-vault.yml 
```	

### Following variable values are set from ansible vault

```
mysql_passwd: tech@123
redis_passwd: tech@123
am_smtp_auth_passwd: BH+efjefkjewhkjq83idhceYkgwBysiio+dGCQVBl3-dummy
slack_url: https://hooks.slack.com/services/T0KU8C7UM/B02V6DE92E5/9Dn72ekjdekqjwqDtRfF##
email_webhook_url: https://alerthistory.techpartner.in/purplle##
squadcast_url: https://api.squadcast.com/v1/incidents/prometheus/c6afkejdqhwkj584441f8f8c123919d5e8d04##
deadmans_switch_url: https://hc-ping.com/bfbd7841-1ed2-41dd-ae13-ec80ekjwfhejbq8##
email_passwd: xyz
```


* Update the monitoring.yml file as per the exporters required. Optionally this will also be taken care by inventory.


Apply ansible playbook using below command

## For AWS 
```
ansible-playbook -i aws_inventory.yaml monitoring.yml --vault-password-file path-to-vault-file/monitoring-vault.yml --private-key=/root/ansible.pem  -u ubuntu
```
## For GCP
```
ansible-playbook -i gcp_inventory.yaml monitoring.yml --vault-password-file path-to-vault-file/monitoring-vault.yml --private-key=/root/ansible.pem  -u ubuntu
```

